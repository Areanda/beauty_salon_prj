from django.urls import path
import salon.views

urlpatterns = [
    path("", salon.views.root_handler),
    path("services/", salon.views.services_handler),
    path("services/<int:service_id>/", salon.views.service_id_handler),
    path("specialists/", salon.views.specialists_handler),
    path("specialists/<int:specialist_id>/", salon.views.specialist_id_handler),
    path("booking/", salon.views.booking_handler)
]
