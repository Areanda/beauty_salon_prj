from django.http import HttpResponse
from django.shortcuts import render


def root_handler(request):
    return HttpResponse(request, 'main_page.html')


def services_handler(request):
    return HttpResponse('All services')


def service_id_handler(request, service_id):
    return HttpResponse('Service information with id %s' % service_id)


def specialists_handler(request):
    return HttpResponse('All specialists')


def specialist_id_handler(request, specialist_id):
    return HttpResponse(f'Information about specialist with id %s' % specialist_id)


def booking_handler(request):
    return HttpResponse('Booking')
