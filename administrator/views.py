from django.http import HttpResponse
from django.shortcuts import render


def root_handler(request):
    return HttpResponse('Hello, admin!')


def bookings(request):
    return HttpResponse('Bookings')


def specialists(request):
    return HttpResponse('Specialists')


def single_specialist(request, specialist_id):
    return HttpResponse('Specialist with id %s' % specialist_id)