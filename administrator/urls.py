from django.urls import path
import administrator.views

urlpatterns = [
    path("", administrator.views.root_handler),
    path("bookings/", administrator.views.bookings),
    path("specialists/", administrator.views.specialists),
    path("specialists/<int:specialist_id>/", administrator.views.single_specialist)
]
